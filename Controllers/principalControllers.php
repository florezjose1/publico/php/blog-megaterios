<?php 

	require_once __DIR__ .  '/../Database/Conexion/conexion.php';

	date_default_timezone_set('America/Bogota');
	
	class PrincipalController extends Conexion {

		public function Registro($query){
			$result = $this->ejectuarMegaterios($query);
			if($result){
			 	return true;
			}else{
				return false;
			}
		}

		public function infoblogs(){
			$fecha = date('Y-m-01 00:00:00');
			$query ="SELECT * FROM blog where `fecha` >= '$fecha' ORDER BY me_gusta DESC";
			$result = $this->ejectuarMegaterios($query);
			if(mysqli_num_rows($result) > 0){
			 	return $result;
			}else{
				return false;
			}
		}
		public function infoblogsPersonales($cod){
			
			$query ="SELECT * FROM blog WHERE usuario = '$cod' ORDER BY me_gusta DESC";
			$result = $this->ejectuarMegaterios($query);
			if(mysqli_num_rows($result) > 0){
			 	return $result;
			}else{
				return false;
			}
		}
		public function infoUsuario($usu){
			$query ="SELECT * FROM usuarios WHERE id = '$usu'";
			$result = $this->ejectuarMegaterios($query);
			if(mysqli_num_rows($result) > 0){
			 	return $result;
			}else{
				return false;
			}
		}
		public function infoRespuestas($blog){
			$query ="SELECT * FROM comentarios WHERE blog = '$blog'";
			$result = $this->ejectuarMegaterios($query);
			if(mysqli_num_rows($result) > 0){
			 	return $result;
			}else{
				return false;
			}
		}


		public function iniSesion($request) {
			$usuario = htmlentities(addslashes($request->usuario));
			$pass    = htmlentities(addslashes($request->pass));
			$cod     = htmlentities(addslashes($request->cod));
			
			$npas = MD5($pass);
			$query ="SELECT * FROM usuarios WHERE `email` = '$usuario' AND `password` = '$npas'   ";
            $result = $this->ejectuarMegaterios($query);
	        $post = array();
            if(mysqli_num_rows($result) > 0){
	            while ($row = mysqli_fetch_object($result)) {
		            session_start();
		            $page = '';
		            $post[] = array( 'detalle'=>1, 'id'=>$row->id, 'page'=>$page);
	                $_SESSION['id'] = $row->id;

	                if ($cod > 0) {
			           	$_SESSION['cod'] = $cod;
			        }
	                
	                $_SESSION['nombre'] = $row->nombre;
	            }
            }else{
            	$post[] = array( 'detalle'=>0);
            }
	        echo json_encode($post);
		}

		public function registroUsuario($request){
			
			$email    = htmlentities(addslashes($request->email));
			$variable = htmlentities(addslashes($request->variable));

			if ($variable == 1) {
				$query ="SELECT * FROM usuarios WHERE email = '$email'";

	            $result = $this->ejectuarMegaterios($query);
	            while ($row = mysqli_fetch_object($result)) {
	                $post[]= array( 'id'=>$row->id  );
	                if ($post != 0 ) {
	                    echo true;
	                }else{
	                    echo 0;
	                }
	            }            
			}
			if($variable == 2){
				
				$query ="SELECT * FROM `usuarios` WHERE `email` ='$email' ";
		        $result = $this->ejectuarMegaterios($query);
			    $post = array();
		        if(mysqli_num_rows($result) > 0){
			       	while ($row = mysqli_fetch_object($result)) {
				       session_start();
				       $page = '';
				       $post[] = array( 'detalle'=>1, 'id'=>$row->id, 'page'=>$page);
			           $_SESSION['id'] = $row->id;
			           $_SESSION['nombre'] = $row->nombre;
			       	}
				}else{
					$post[] = array( 'detalle'=>0);
				}				
			}
			echo json_encode($post);

		}


		public function registroblog($request){
			
			$blog    = htmlentities(addslashes($request->blog));
			$usuario = htmlentities(addslashes($request->usuario));

			$fecha = date('Y-m-d H:i:s');

			$query = "INSERT INTO `blog` (`id`, `content`, `fecha`, `usuario`, `me_gusta`) VALUES (NULL, '$blog', '$fecha', '$usuario', '')";
			$result = $this->ejectuarMegaterios($query);

			if ($result) {
				echo true;
					
			}else{
				echo false;
			}				
		}
		public function repuestaBlog($request){
			
			$blog       = htmlentities(addslashes($request->blog));
			$comentario = htmlentities(addslashes($request->comentario));
			$usuario    = htmlentities(addslashes($request->usuario));

			$fecha = date('Y-m-d H:i:s');

			$query = "INSERT INTO `comentarios`(`id`, `blog`, `comentario`, `usuario`, `fecha`, `me_gusta`) VALUES (NULL, '$blog', '$comentario', '$usuario', '$fecha', '')";
			$result = $this->ejectuarMegaterios($query);

			if ($result) {
				echo true;
					
			}else{
				echo false;
			}				

		}
		public function informacionComentarios($request){
			$id = $request->id;

			$query ="SELECT * FROM blog WHERE `id` = '$id'   ";
			$result = $this->ejectuarMegaterios($query);
	        $post = array();
            if(mysqli_num_rows($result) > 0){
	            while ($row = mysqli_fetch_object($result)) {
					$post[] = array( 'detalle'=>1, 'id'=>$row->id, 'content'=>$row->content);
				}
            }else{
            	$post[] = array( 'detalle'=>0);
            }
	        echo json_encode($post);
		}
		public function updateComentario($request){
			$id      = htmlentities(addslashes($request->id));
			$content = htmlentities(addslashes($request->content));

			$query="UPDATE `blog` SET `content`='$content' WHERE `id` = '$id' ";
				

			$result = $this->ejectuarMegaterios($query);
			if ($result) {
				echo true;
			}else{
				echo false;
			}
		}
		public function deleteComentario($request){
			$id      = htmlentities(addslashes($request->id));

			$query="DELETE FROM `blog` WHERE `id` = '$id' ";

			$result = $this->ejectuarMegaterios($query);
			if ($result) {
				echo true;
			}else{
				echo false;
			}
		}





	}
?>