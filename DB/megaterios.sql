-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-06-2017 a las 19:36:21
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `megaterios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

CREATE TABLE `blog` (
  `id` int(100) NOT NULL,
  `content` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `usuario` int(100) NOT NULL,
  `me_gusta` int(100) NOT NULL,
  `foto` varchar(250) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `blog`
--

INSERT INTO `blog` (`id`, `content`, `fecha`, `usuario`, `me_gusta`, `foto`) VALUES
(1, 'Â¿QuÃ© es un banco de proyectos?\r\nEs una herramienta del sistema de planificaciÃ³n sobre la inversiÃ³n pÃºblica que permite tomar decisiones en la etapa de pre-inversiÃ³n e inversiÃ³n, facilitando la preparaciÃ³n de los planes, programas y proyectos de inversiÃ³n, racionalidad y consistencia en la asignaciÃ³n del presupuesto para cada vigencia. [1]\r\n[2] Es una herramienta que permite: \r\nâ€¢	Tomar decisiones a lo largo del ciclo de vida de la inversiÃ³n.\r\nâ€¢	Mejorar la eficiencia de la inversiÃ³n pÃºblica.\r\nâ€¢	Permite monitorear y hacer seguimiento de las inversiones.\r\n', '2017-06-03 11:55:09', 1, 0, '8968f50eb517.jpg'),
(2, 'Â¿Para quÃ© sirve?\r\n\r\nNos permite identificar, formular y evaluar los proyectos, la programaciÃ³n y la asignaciÃ³n de los recursos, el seguimiento  a la ejecuciÃ³n y hacer la evaluaciÃ³n de los resultados obtenidos.\r\nAdemÃ¡s permite:\r\nâ€¢	Articular el proceso de planeaciÃ³n.\r\nâ€¢	Elaborar un presupuesto de inversiÃ³n pÃºblica. \r\nâ€¢	Y orientar el gasto a satisfacer las necesidades de la poblaciÃ³n.\r\n', '2017-06-03 12:05:26', 2, 0, '1cd59a0b5fc3.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `id` int(100) NOT NULL,
  `blog` int(100) DEFAULT NULL,
  `comentario` varchar(1000) COLLATE utf8mb4_spanish_ci NOT NULL,
  `usuario` int(100) NOT NULL,
  `fecha` datetime NOT NULL,
  `me_gusta` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`id`, `blog`, `comentario`, `usuario`, `fecha`, `me_gusta`) VALUES
(1, 2, 'ggggggggggggggg', 2, '2017-06-03 12:12:11', 0),
(2, 1, '&iquest;Para qu&eacute; sirve? Nos permite identificar, formular y evaluar los proyectos, la programaci&oacute;n y la asignaci&oacute;n de los recursos, el seguimiento a la ejecuci&oacute;n y hacer la evaluaci&oacute;n de los resultados ob', 2, '2017-06-03 12:34:46', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(100) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `foto` varchar(300) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `fecha_nacimiento`, `email`, `password`, `foto`) VALUES
(1, 'Jose Rodolfo', '1997-09-25', 'rojosefo132@gmail.com', '9250edb343851abb03b206937ab92f11', 'c1539224cd19.jpg'),
(2, 'Ana', '1990-01-20', 'ana@gmail.com', '276b6c4692e78d4799c12ada515bc3e4', '3f7ae1086f4b.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario` (`usuario`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog` (`blog`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `blog_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `comentarios_ibfk_1` FOREIGN KEY (`blog`) REFERENCES `blog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
