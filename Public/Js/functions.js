function iniciarSesion() {
	var usuario= $('#email-user').val();
	var pass   = $('#pass-user').val();
	var cod = $('#cod_respuesta').val();
	console.log("cod", cod);
	var param  ={'Funcion':'iniSesion', 'usuario':usuario, 'pass':pass, 'cod':cod};
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: 'ajax.php',
        success: function(data) {
            console.log("data", data);
        	var json = JSON.parse(data);
        	var a = json[0].detalle;
        	if (a > 0 ) {
        		var page = 'Views/'+json[0].page;
        		window.location=page;
        	}else{
                var title = "Procesando";
				var ms = "Error!";
				var mensaje = "Usuario o contraseña incorrecto.";
				error(title, ms, mensaje);
        	}
        }
    });
}
function registroUsuario(){
	var nombre = $('#nombreRegistro').val();
	var fechaN = $('#FNRegistro').val();
	var email = $('#emailRegistro').val();
	var pass = $('#passwordRegistro').val();
	// validamos email
	if (nombre.length != 0  && email.length != 0 && pass.length != 0 ) {
		var param  ={'Funcion':'registroUsuario', 'variable':'1', 'email':email };
		$.ajax({
		    data: JSON.stringify(param),
		    type: "JSON",
		    url: 'ajax.php',
		    success: function(data) {

		    	if (data > 0) {
		    		var title = "Procesando";
		    		var ms = "Error!";
		    		var mensaje = "El usuario ya existe.!";
		    		error(title, ms, mensaje);
		    	}else{

		    		var formData= new FormData(document.forms.namedItem("formularioRegistroUsuarios"));
				    var parametro = { 'formData':formData};
				    $.ajax({
				        data: formData,
				        type: 'POST',
				        url: 'Controllers/registroUsuarios.php',
				        cache: false,
				        contentType: false,
				        processData: false,
				        success:function(resultado){

					    	if (resultado != 0 ) {
					    		var email = $('#emailRegistro').val();
					    		var param  ={'Funcion':'registroUsuario', 'variable':'2', 'email':email };
								$.ajax({
								    data: JSON.stringify(param),
								    type: "JSON",
								    url: 'ajax.php',
								    success: function(data) {
								    	if (data != 0) {
								    		var mensaje = "Procesando...!";
								    		var head = "Registrado";
								    		var text = "El usuario ha sido registrado...";
								    		success(mensaje,head,text);
								    		setTimeout(function(){
								    			$('#cerrarModalRegistro').click();
								    			location.reload(true);
								    		},1500);
								    	}else{
								    		var title = "Procesando";
								    		var ms = "Error!";
								    		var mensaje = "El usuario no se ha podido registrar";
								    		error(title, ms, mensaje);
								    	}
								    }
								});
					    	}else{
					    		var title = "Procesando";
					    		var ms = "Error!";
					    		var mensaje = "El usuario no se ha podido registrar";
					    		error(title, ms, mensaje);
					    	}
				        }
				    });
		    	}
		    }
		});
	}else{
		var title = "Procesando";
		var ms = "Error!";
		var mensaje = "El usuario no ha sido registrado";
		error(title, ms, mensaje);
	}
}

function publicarBlog(id){
	var blog = $('#comment-blog').val();
	console.log("$blog", blog);

	if (blog.length != 0  ) {
		var formData= new FormData(document.forms.namedItem("formularioBlog"));
		var parametro = { 'formData':formData};
		$.ajax({
			data: formData,
			type: 'POST',
			url: '../Controllers/registroBlog.php',
			cache: false,
			contentType: false,
			processData: false,
			success:function(resultado){

				if (resultado != 0 ) {
					var mensaje = "Procesando...!";
					var head = "Registrado";
					var text = "Tu publicacion ha sido registrada...";
					success(mensaje,head,text);
					setTimeout(function(){
						location.reload(true);
					},1500);
				}else{
					var title = "Procesando";
					var ms = "Error!";
					var mensaje = "No se ha podido publicar";
					error(title, ms, mensaje);
				}
			}
		});
	}else{
		var title = "Procesando";
		var ms = "Error!";
		var mensaje = "No se ha podido publicar";
		error(title, ms, mensaje);
	}
}

function abrirResponder(id){
    console.log("id", id);
	$('#responderBlog_'+id).show('slow');
	$('#responderBlog_'+id).css('display','block');
	$('#btnOpcionResponder').hide('slow');
}
function responderBlog(usu,blog){
	var comentario = $('#comentario-blog_'+blog).val();

	if (blog.length != 0  ) {
		var param  ={'Funcion':'repuestaBlog', 'comentario':comentario, 'usuario':usu, 'blog':blog };
		$.ajax({
		    data: JSON.stringify(param),
		    type: "JSON",
		    url: '../ajax.php',
		    success: function(data) {
		    	console.log("data", data);
				if (data != 0 ) {
					var mensaje = "Procesando...!";
					var head = "Registrado";
					var text = "Tu comentario ha sido registrado...";
					success(mensaje,head,text);
					setTimeout(function(){
						location.reload(true);
					},1500);
				}else{
					var title = "Procesando";
					var ms = "Error!";
					var mensaje = "No se ha podido publicar";
					error(title, ms, mensaje);
				}
		    }
		});
	}else{
		var title = "Procesando";
		var ms = "Error!";
		var mensaje = "No se ha podido publicar";
		error(title, ms, mensaje);
	}
}

function detallePublicacion(id){
	$('#btndetallePublicacion').click();
	$('#id_publicacionUpdate').val(id);

	var param  ={'Funcion':'informacionComentarios', 'id':id};
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: '../ajax.php',
        success: function(data) {
            console.log("data", data);
        	var json = JSON.parse(data);

        	var content = json[0].content;

        	$('#Updatecomentario-blog').val(content)
        }
    });
}
function updateComentario(){
	var id = $('#id_publicacionUpdate').val();
	console.log("id", id);
	var content = $('#Updatecomentario-blog').val();
	console.log("content", content);

	var param  ={'Funcion':'updateComentario', 'id':id, 'content':content};
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: '../ajax.php',
        success: function(data) {
            if (data != 0) {
				var mensaje = "Procesando...!";
				var head = "Actualizado";
				var text = "Tu comentario ha sido actualizado...";
				success(mensaje,head,text);
				setTimeout(function(){
					location.reload(true);
				},3500);
			}else{
				var title = "Procesando";
				var ms = "Error!";
				var mensaje = "No se ha podido actualizar... vuelve a intentarlo.!";
				error(title, ms, mensaje);
			}
        }
    });
}
function deleteComentario(){
	var id = $('#id_publicacionUpdate').val();
	console.log("id", id);

	var param  ={'Funcion':'deleteComentario', 'id':id };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: '../ajax.php',
        success: function(data) {
            console.log("data", data);
            if (data != 0) {
				var mensaje = "Procesando...!";
				var head = "Eliminado";
				var text = "Tu comentario ha sido Eliminado...";
				success(mensaje,head,text);
				setTimeout(function(){
					location.reload(true);
				},3500);
			}else{
				var title = "Procesando";
				var ms = "Error!";
				var mensaje = "No se ha podido actualizar... vuelve a intentarlo.!";
				error(title, ms, mensaje);
			}
        }
    });
}

// hacemos que el usuario inicie sesion para que así este pueda hacer los respesctivos comentarios.
function validarSesion(){
	console.log('..............')
	$('#iniciarSesion').click();
}


function success(menj, head, texto) {
    $.toast({
        text: menj,
        textAlign: 'center',
        bgColor: '#59ad85',
        position: 'bottom-right'
    })
    setTimeout(function(){
        $.toast({
            heading: head,
            text: texto,
            showHideTransition: 'slide',
            position: 'bottom-right',
            icon: 'success'
        })
    },2000);
}
function error(title, ms, mensaje) {
    var myToast = $.toast({
        heading: title,
        text: ms,
        icon: 'info',
        position: 'bottom-right',
        hideAfter: false
    });

    // Update the toast after three seconds.
    window.setTimeout(function(){
        myToast.update({
            heading: 'Error',
            text: mensaje,
            icon: 'error',
            position: 'bottom-right',
            hideAfter: false,
            showHideTransition: 'slide',
        });
    }, 2000)
}
















