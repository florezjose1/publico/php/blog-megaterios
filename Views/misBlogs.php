<?php
    session_start();
    
    if(isset($_SESSION['cod'])) {
    	$respuesta = $_SESSION['cod'];

    }else{
    	$respuesta = 999;
    }


    if(isset($_SESSION['id'])) {
      $id = $_SESSION['id'];
      $nombre = $_SESSION['nombre'];


    require_once '../Controllers/principalControllers.php';
   	require_once '../Config/vars.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Megaterios</title>
	<link rel="stylesheet" href="../Public/css/style.css">
	<link rel="stylesheet" href="../Public/css/bootstrap.css">
	<link rel="stylesheet" href="../Public/css/compiled.css">
	<link rel="stylesheet" href="../Public/css/AdminLTE.min.css">
	<link rel="stylesheet" href="../Public/css/font-awesome.min.css">
	<link rel="stylesheet" href="../Public/css/ionicons.min.css">
	<link rel="stylesheet" href="../Public/css/jquery.toast.css">
</head>
<body>
	<div class="container-fluid">

		<div class="header">
			<div class="div col-md-4"></div>
			<div class="col-xs-12 col-md-4">
				<h2 ><a href="">Blog - Megaterios </a> </h2>
			</div>
			<div class="div col-md-4">
				<div class="menu">
					<a data-toggle="modal" data-target="#modal-sesion"><?php echo $nombre; ?></a>
					<a href="logout.php">Cerrar Sesion</a>
				</div>
			</div>
		</div>

		<div class="container" id="publicarBlog" >
			
			<div class="row">
				<h2>Blog</h2>
					<form name="formularioBlog" enctype="multipart/form-data" method="post">
						<textarea class="form-control" rows="1" name="comment-blog" id="comment-blog" placeholder="Escribe tu publicacion"></textarea>
						<input type="file" name="imgPublicacion">
						<input type="hidden" name="usuarioPublicacion" value="<?php echo $id ?>">
					</form>

					<button class="btn btn-default" style="float: right;" onclick="publicarBlog(<?php echo $id ?>)">Publicar</button>

				<div class="col-md-12">
					<center>
					
					<div class="col-md-6" id="publicPublicas" style="background-color: #eee;padding: 10px;">
						<a href="index.php">
						<h3>Publicaciones Publicas</h3>
						</a>
					</div>
					<div class="col-md-6" id="misPublicaiones" style="background-color: #eee;padding: 10px;">
						<a href="misBlogs.php">
							<h3>Mis Publicaciones</h3>
						</a>
					</div>


					</center>

				</div>
			</div>
		</div>
		<div class="container">
			<hr>
			<center><h3>Publicaciones Publicas</h3></center>
			<hr>
			<div class="row">
				<?php 



					$consulta = new PrincipalController();

					$blogs = $consulta->infoblogsPersonales($id);


                    if ($blogs != false) {
                      	while ($blog = mysqli_fetch_object($blogs)) {
                      		$usu = $blog->usuario;
                      		$fechaPub = $blog->fecha;
                      		$hoy = date('Y-m-d H:i:s');

                      		if ($hoy == $fechaPub) {
                      			$fecha1 = new DateTime($fechaPub);
								$fecha2 = new DateTime($hoy);
								$fechaDif = $fecha1->diff($fecha2);

								$fecha_publicacion =  $fechaDif->h.':'. $fechaDif->i;
                      		}else{

                      			$fecha1 = new DateTime($fechaPub);
								$fecha2 = new DateTime($hoy);
								$fechaDif = $fecha1->diff($fecha2);

                      			if ($fechaDif->y > 0 ) {
	                      			$fecha_publicacion = $fechaDif->y.':'.$fechaDif->m.':'.$fechaDif->d;
                      			}else{
	                      			if ($fechaDif->m > 0 ) {
                      					$fecha_publicacion = $fechaDif->m.' meses '.$fechaDif->d . ' dias';
	                      			}else{
	                      				if ($fechaDif->d > 1 ) {
	                      					$fecha_publicacion = $fechaDif->d . ' dias';
	                      				}else{
	                      					$fecha_publicacion = $fechaDif->h . ' hora ' . $fechaDif->h .' min';
	                      				}

	                      			}
                      			}
                      		}


                      		$usuario = $consulta->infoUsuario($usu);
		                    if ($usuario != false) {
		                      	while ($user = mysqli_fetch_object($usuario)) {
		                      		$name = $user->nombre;
		                      		$foto = $user->foto;
		                      	}
		                      }

                ?>

			                <div class="box-body chat" id="chat-box">
								<div class="item">
				                	<img src="../Public/img/usuarios/<?php echo $foto; ?>" alt="user image" class="offline">
						            <p class="message">
						            <a href="#" class="name">	
						                <small class="text-muted pull-right">
						                	<i class="fa fa-clock-o"></i> <?php echo $fecha_publicacion; ?>
						                	<a onclick="detallePublicacion(<?php echo $blog->id ?>)" style="margin-right: 10px;cursor: pointer;"><i class="fa fa-info" aria-hidden="true"></i></a>
						                </small>
						                
						                <?php echo $name ?>
						            </a>
						            	<div class="row">
						            		<div class="col-xs-12 col-md-12">
						            			<center><img class="img-responsive" id="img-publicacion" src="../Public/img/blog/<?php echo $blog->foto; ?>" alt="" ></center>
						            		</div>
						            		<div class="col-xs-12 col-md-12">
						                		<?php echo $content = $blog->content; ?>
						                		<hr>
						                		<a style="cursor: pointer;" id="btnOpcionResponder" onclick="abrirResponder(<?php echo  $blog->id; ?>)">Responder</a>
						            		</div>
						            	</div>
						            	
						            </p>

						            <?php 

						            	$InfoRespuestas = $consulta->infoRespuestas($blog->id);
					                    if ($InfoRespuestas != false) {
					                      	while ($respueta = mysqli_fetch_object($InfoRespuestas)) {
					                      		$usu = $respueta->usuario;

					                      		// calculo de tiempo
					                      		$fechaResp = $respueta->fecha;
					                      		$hoy = date('Y-m-d H:i:s');

					                      		if ($hoy == $fechaResp) {
					                      			$fecha1 = new DateTime($fechaResp);
													$fecha2 = new DateTime($hoy);
													$fechaDif = $fecha1->diff($fecha2);

													$fecha_Repuesta =  $fechaDif->h.':'. $fechaDif->i;
					                      		}else{

					                      			$fecha1 = new DateTime($fechaResp);
													$fecha2 = new DateTime($hoy);
													$fechaDif = $fecha1->diff($fecha2);

					                      			if ($fechaDif->y > 0 ) {
						                      			$fecha_Repuesta = $fechaDif->y.':'.$fechaDif->m.':'.$fechaDif->d;
					                      			}else{
						                      			if ($fechaDif->m > 0 ) {
					                      					$fecha_Repuesta = $fechaDif->m.' meses '.$fechaDif->d . ' dias';
						                      			}else{
						                      				if ($fechaDif->d > 1 ) {
						                      					$fecha_Repuesta = $fechaDif->d . ' dias';
						                      				}else{
						                      					$fecha_Repuesta = $fechaDif->h . ' hora ' . $fechaDif->h .' min';
						                      				}

						                      			}
					                      			}
					                      		}


						            ?>
										
										<div class="attachment">
											<div class="item">
												<?php 
													$usuario = $consulta->infoUsuario($usu);
								                    if ($usuario != false) {
								                      	while ($user = mysqli_fetch_object($usuario)) {
								                      		$name = $user->nombre;
								                      		$foto = $user->foto;
								                      		echo '
								                      			<img src="../Public/img/usuarios/'.$foto.'" alt="user image" class="offline">
								                      		';
								                      	}
								                    }
												?>
												<p class="message">
												<a href="#" class="name">	
									                <small class="text-muted pull-right">
									                <i class="fa fa-clock-o"></i> <?php echo $fecha_Repuesta; ?></small>
									                    <?php echo $name ?>
									            </a>
								                    
								                      <?php echo $respueta->comentario;?>
								                </p>
						                    </div>
						                </div>

						            <?php 
						            		}// while de repuestas
						            		echo '
											<div class="attachment" id="responderBlog_'.$blog->id.'" >
						                    	<textarea class="form-control" rows="2" id="comentario-blog_'.$blog->id.'"></textarea>
							                	<div class="pull-right">
							                	<button type="button" class="btn btn-primary btn-sm btn-flat" onclick="responderBlog('.$id.','.$blog->id.')">Responder</button>
								                </div>
							                </div>';
						            	}else{
						            ?>
						            <div class="attachment" id="responderBlog_<?php echo $blog->id;?>" style="display: none;">
					                    <textarea class="form-control" rows="2" id="comentario-blog_<?php echo $blog->id;?>"></textarea>
						                <div class="pull-right">
						                	<button type="button" class="btn btn-primary btn-sm btn-flat" onclick="responderBlog(<?php echo $id;?>,<?php echo  $blog->id; ?>)">Responder</button>
						                </div>
					                </div>
					                <?php 
					                	} // else de no respuestas
					                ?>

				                </div>
				            </div>
	            <?php 
			        	} // while blog
			    	}// if blog
	            ?>
			</div>
		</div>
	</div>

<!-- Modal sesion -->
<button data-toggle="modal" data-target="#modal-detallePublicacion" id="btndetallePublicacion" style="display: none;"></button>
<div class="modal fade modal-ext" id="modal-detallePublicacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content modal-md">
            <!--Header-->
            <center>
	            <button class="btn btn-success" data-toggle="modal" data-target="#modal-updatePublicacion" data-dismiss="modal">Actualizar</button>
	            <button class="btn btn-danger" data-toggle="modal" data-target="#modal-eliminarPublicacion" data-dismiss="modal">Eliminar</button>
            </center>
        </div>
        <!--/.Content-->
    </div>
</div>

<!-- Modal actualizar -->
<div class="modal fade modal-ext" id="modal-updatePublicacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content modal-md">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="w-100"><i class="fa fa-user"></i> Actualizar <p id="publicacionUpdate"></p></h3>
            </div>
            <!--Body-->
            <div class="modal-body">
                <div id="fomulario" style="display: block;">
						<input type="hidden" id="id_publicacionUpdate" value="0">
					<center>
						<textarea class="form-control" rows="10" id="Updatecomentario-blog"></textarea>


						
						<br>
						<button class="btn btn-primary" onclick="updateComentario()">Actualizar</button>
					</center>
                </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="button" id="cerrarModalSesion" class="btn btn-danger btn-sm ml-auto" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Modal eliminar -->
<div class="modal fade modal-ext" id="modal-eliminarPublicacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content modal-md">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="w-100"><i class="fa fa-user"></i> Eliminar</h3>
            </div>
            <!--Body-->
            <div class="modal-body">
                <div id="fomulario" style="display: block;">
						<input type="hidden" id="id_publicacionUpdate" value="0">
					<center>
						<h3>¿Estas seguro de elimnar comentario?</h3>						
						<br>
						<button class="btn btn-primary" onclick="deleteComentario()">Eliminar</button>
					</center>
                </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="button" id="cerrarModalSesion" class="btn btn-danger btn-sm ml-auto" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>


	
	<script src="../Public/Js/jquery-1.9.1.min.js"></script>
	<script src="../Public/Js/functions.js"></script>
	<script src="../Public/Js/bootstrap.js"></script>
	<script src="../Public/Js/jquery.toast.js"></script>

	
</body>
</html>

<?php
   }else{
      echo '<script> window.location="../index.php"; </script>';
   }
?>