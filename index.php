<?php
	date_default_timezone_set('America/Bogota');

    session_start();
    if(isset($_SESSION['id'])) {
        echo '<script> window.location="Views/"; </script>';
    }

   

   require_once 'Controllers/principalControllers.php';
   require_once 'Config/vars.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Blog</title>
	<link rel="stylesheet" href="Public/css/style.css">
	<link rel="stylesheet" href="Public/css/bootstrap.css">
	<link rel="stylesheet" href="Public/css/compiled.css">
	<link rel="stylesheet" href="Public/css/AdminLTE.min.css">
	<link rel="stylesheet" href="Public/css/font-awesome.min.css">
	<link rel="stylesheet" href="Public/css/ionicons.min.css">
	<link rel="stylesheet" href="Public/css/jquery.toast.css">
</head>
<body>
	<div class="container-fluid">

		<div class="header">
			<div class="div col-md-4"></div>
			<div class="col-xs-12 col-md-4">
				<h2 ><a href="">Blog - Megaterios </a></h2>
			</div>
			<div class="div col-md-4">
				<div class="menu">
					<a id="iniciarSesion" data-toggle="modal" data-target="#modal-sesion">Iniciar Sesion</a>
					<a id="Registrarse" data-toggle="modal" data-target="#modal-registro">Registrate</a>
				</div>
			</div>	
		</div>


		<div class="container">	
			<!--<div class="row">
				<div class="col-xs-12">
					<center>
						<img src="Public/img/one-eye-transparent.png" alt="" class="img-responsive" width="300px">
					</center>
				</div>
			</div>
				<center><h1>Publicaciones</h1></center>
				<hr>
			<hr>-->
			
			<div class="row">
				<?php 

					$consulta = new PrincipalController();

					$blogs = $consulta->infoblogs();
                    if ($blogs != false) {
                      	while ($blog = mysqli_fetch_object($blogs)) {
                      		$usu = $blog->usuario;
                      		$fechaPub = $blog->fecha;
                      		$hoy = date('Y-m-d H:i:s');

                      		if ($hoy == $fechaPub) {
                      			$fecha1 = new DateTime($fechaPub);
								$fecha2 = new DateTime($hoy);
								$fechaDif = $fecha1->diff($fecha2);

								$fecha_publicacion =  $fechaDif->h.':'. $fechaDif->i;
                      		}else{

                      			$fecha1 = new DateTime($fechaPub);
								$fecha2 = new DateTime($hoy);
								$fechaDif = $fecha1->diff($fecha2);

                      			if ($fechaDif->y > 0 ) {
	                      			$fecha_publicacion = $fechaDif->y.':'.$fechaDif->m.':'.$fechaDif->d;
                      			}else{
	                      			if ($fechaDif->m > 0 ) {
                      					$fecha_publicacion = $fechaDif->m.' meses '.$fechaDif->d . ' dias';
	                      			}else{
	                      				if ($fechaDif->d > 1 ) {
	                      					$fecha_publicacion = $fechaDif->d . ' dias';
	                      				}else{
	                      					$fecha_publicacion = $fechaDif->h . ' hora ' . $fechaDif->h .' min';
	                      				}

	                      			}
                      			}
                      		}


                      		$usuario = $consulta->infoUsuario($usu);
		                    if ($usuario != false) {
		                      	while ($user = mysqli_fetch_object($usuario)) {
		                      		$name = $user->nombre;
		                      		$foto = $user->foto;
		                      	}
		                      }

                ?>

			                <div class="box-body chat" id="chat-box">
								<div class="item">
				                	<img src="Public/img/usuarios/<?php echo $foto; ?>" alt="user image" class="offline">
						            <p class="message">
						            <a href="#" class="name">	
						                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?php echo $fecha_publicacion; ?></small>
						                    <?php echo $name ?>
						            </a>
						            	<div class="row">
						            		<div class="col-xs-12 col-md-12">
						            			<center><img class="img-responsive" id="img-publicacion" src="Public/img/blog/<?php echo $blog->foto; ?>" alt="" ></center>
						            		</div>
						            		<div class="col-xs-12 col-md-12">
						                		<?php echo $content = $blog->content; ?>
						                		<hr>
						                		<a style="cursor: pointer;" id="btnOpcionResponder" onclick="abrirResponder(<?php echo  $blog->id; ?>)">Responder</a>
						            		</div>
						            	</div>
						            	<center>
						            	</center>
										<br>
						            	
						            </p>

						            <?php 

						            	$InfoRespuestas = $consulta->infoRespuestas($blog->id);
					                    if ($InfoRespuestas != false) {
					                      	while ($respueta = mysqli_fetch_object($InfoRespuestas)) {
					                      		$usu = $respueta->usuario;
					                      		$fecha_Repuesta = $respueta->fecha;

						            ?>

						            	<div class="attachment">
											<div class="item">
												<?php 
													$usuario = $consulta->infoUsuario($usu);
								                    if ($usuario != false) {
								                      	while ($user = mysqli_fetch_object($usuario)) {
								                      		$name = $user->nombre;
								                      		$foto = $user->foto;
								                      		echo '
								                      			<img src="Public/img/usuarios/'.$foto.'" alt="user image" class="offline">
								                      		';
								                      	}
								                    }
												?>
												<p class="message">
												<a href="#" class="name">	
									                <small class="text-muted pull-right">
									                <i class="fa fa-clock-o"></i> <?php echo $fecha_Repuesta; ?></small>
									                    <?php echo $name ?>
									            </a>
								                    
								                    <?php echo $respueta->comentario;?>
								                </p>
						                    </div>
						                </div>



						            <?php 
						            		}// while de repuestas
						            		echo '
											<div class="attachment" id="responderBlog_<?php echo $blog->id;?>" >
						                    	<textarea onclick="validarSesion()" class="form-control" rows="2" id="comentario-blog"></textarea>
							                	<div class="pull-right">
							                	<button type="button" class="btn btn-primary btn-sm btn-flat" data-toggle="modal" data-target="#modal-sesion">Responder</button>
								                </div>
							                </div>';
						            	}else{
						            ?>
						            <div class="attachment" id="responderBlog_<?php echo $blog->id;?>" style="display: none;">
					                    <textarea onkeyup="validarSesion()" onclick="validarSesion()" class="form-control" rows="2" id="comentario-blog"></textarea>
						                <div class="pull-right">
						                	<button type="button" class="btn btn-primary btn-sm btn-flat" onclick="responderBlog(<?php echo $id;?>,<?php echo  $blog->id; ?>)">Responder</button>
						                </div>
					                </div>
					                <?php 
					                	} // else de no respuestas
					                ?>

				                </div>
				                <div class="col-xs-12"><hr></div>
				            </div>
	            <?php 
			        	} // while blog
			    	}// if blog
	            ?>

					
			</div>
		</div>
	</div>


<!-- Modal sesion -->
<div class="modal fade modal-ext" id="modal-sesion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content modal-md">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="w-100"><i class="fa fa-user"></i> Iniciar Sesion</h3>
            </div>
            <!--Body-->
            <div class="modal-body">
                <div id="fomulario" style="display: block;">
						<input type="hidden" id="cod_respuesta" value="0">
					<center>
						<p>Email</p>
						<input type="email" id="email-user" style="width: 90%">
						
						<p>Contraseña</p>
						<input type="password" id="pass-user" style="width: 90%">
						
						<br>
						<button class="btn btn-primary" onclick="iniciarSesion()">Iniciar Sesion</button>
					</center>
                </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="button" id="cerrarModalSesion" class="btn btn-danger btn-sm ml-auto" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>

<!-- Modal registro -->
<div class="modal fade modal-ext" id="modal-registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content modal-md">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="w-100"><i class="fa fa-user"></i> Registro Usuario</h3>
            </div>
            <!--Body-->
            <div class="modal-body">
                <div id="fomulario" style="display: block;">
					<center>
					<form name="formularioRegistroUsuarios" enctype="multipart/form-data" method="post">
						<p>Nombre</p>
						<input type="text" name="nombreRegistro" id="nombreRegistro" style="width: 90%">
						
						<p>Fecha Nacimiento</p>
						<input type="date" name="FNRegistro" id="FNRegistro" style="width: 90%">
						
						<p>Email</p>
						<input type="email" name="emailRegistro" id="emailRegistro" style="width: 90%">

						<p>Contraseña</p>
						<input type="password" name="passwordRegistro" id="passwordRegistro" style="width: 90%">

						<p>Foto</p>
						<input type="file" name="fotoRegistro" id="fotoRegistro">
					</form>
						
						<br>
						<button class="btn btn-primary" onclick="registroUsuario()">Registrar</button>
					</center>
                </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="button" id="cerrarModalRegistro" class="btn btn-danger btn-sm ml-auto" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>






	
	<script src="Public/Js/jquery-1.9.1.min.js"></script>
	<script src="Public/Js/functions.js"></script>
	<script src="Public/Js/bootstrap.js"></script>
	<script src="Public/Js/jquery.toast.js"></script>

	
	
</body>
</html>